/**
  * Donnée : Prend un tableau d'entier T et un entier N la taille du tableau
  * Résultat : affecte à *pmin la plus petite valeur de T (ou INT_MAX si n=0) et à *pmax la plus grande valeur de T (ou INT_MIN si n=0).

  */
void minmax(int *t, int n, int *pmin, int *pmax);
