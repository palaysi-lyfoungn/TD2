#include <stdlib.h>

int* unsurdeux(int *tab, int n);

void* malloc(size_t size){
  if (size == 5*sizeof(int)) exit(EXIT_SUCCESS);
  else exit(EXIT_FAILURE);
}

int main(){
  int T[]={1,2,3,4,5,6,7,8,9};
  unsurdeux(T,9);
  return EXIT_FAILURE;
}
