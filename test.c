#include <stdio.h>
#include <malloc.h>

void f(int a, int b, int *s, int *p);

int *copie(int *tab, int n);

void f(int a, int b, int *s, int *p) {
    *s = a + b;
    *p = a * b;
}

int *copie(int *tab, int n){
    int *tab2 = malloc(sizeof(int) * n);
    for (int i = 0; i < n; i++) {
        tab2[i] = tab[i];
    }
    return tab2;
}



int main(){
    int x, y;
    int t[] = {1,2,3,4,5,6,7,8,9,0};
    f(12, 4, &x, &y);
    printf("x = %d, y = %d\n", x, y);
    int *t2 = copie(t,10);
}