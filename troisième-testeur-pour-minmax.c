#include <stdlib.h>

void minmax (int T[], int n, int *min, int *max);

int
main ()
{
  int T[2] = { -1, 1 };
  int n = 2;
  int min, max;
  minmax (T, n, &min, &max);
  if (min == -1 && max == 1)
    return EXIT_SUCCESS;
  else
    return EXIT_FAILURE;
}
