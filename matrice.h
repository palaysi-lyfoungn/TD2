//
// Created by nikholah on 05/03/24.
//

#ifndef TD2_MATRICE_H
#define TD2_MATRICE_H
struct Matrice {
    int nb_lignes;
    int nb_colonnes;
    int **valeurs;
};
#endif //TD2_MATRICE_H
