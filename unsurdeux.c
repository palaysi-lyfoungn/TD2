#include <malloc.h>

int *unsurdeux(int *tab, int n){
    int *tab2 = malloc(sizeof(int) * ((n/2)+1));
    if (tab2 != NULL) {
        int j = 0;
        for (int i = 0; i < n; ++i) {
            if (i % 2 == 0) {
                tab2[j] = tab[i];
                j++;
            }
        }
    }
    return tab2;
}
