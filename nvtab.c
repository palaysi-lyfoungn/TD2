#include <stdio.h>
#include <malloc.h>
#include <stdlib.h> // pour EXIT_SUCCESS et EXIT_FAILURE

/**
* Donnée : prend en parametre un tableau d'entiers et un entier n
* Résultat : retourne une copie des n premiers entiers de T
*/
int *nvtab(int T[], int n){
 	int *tab2 = malloc(sizeof(int) * n);
	if (tab2 == NULL){
        return tab2;
	}
   
    for (int i = 0; i < n; ++i) {
        tab2[i] = T[i];
    }
    return tab2;
}
