#include "minmax.h"
#include <limits.h>

void minmax(int *t, int n, int *pmin, int *pmax){
    *pmax = INT_MIN;
    *pmin = INT_MAX;
    for (int i = 0; i < n; i++){
        if (t[i] > *pmax){
            *pmax = t[i];
        }
        if (t[i] < *pmin){
            *pmin = t[i];
        }
    }
}
